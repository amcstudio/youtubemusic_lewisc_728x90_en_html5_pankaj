'use strict';
~ function() {
    var $ = TweenMax,
        bgExit = document.getElementById('bgExit'),
        ad = document.getElementById('mainContent'),        
        YTIcon = document.getElementById('YTIcon'), 
        YTMusicCopyContainer = document.getElementById('YTMusicCopyContainer'),
        YTMusicCopy = document.getElementById('YTMusicCopy'),
        playNowContainer = document.getElementById('playNowContainer'),
        playNowCopy = document.getElementById('playNowCopy'),
        currentLoop = 1,
        totalLoops = 3,          
        ring = document.getElementById('ring'),
        circle = document.getElementById('circle'),
        tlImage,
        Logo_tl;
    
       
    
        window.init = function() {
            bgExit.addEventListener('click',clickHandler) 

        $.delayedCall(0, play)

      }

    function play() {

        Logo_tl = new TimelineMax();

        //////// YT LOGO //////////////////
        Logo_tl.kill();
        $.set(ring, {opacity:1});
        $.set(circle, {opacity:1});

       /////////// main animation start //////////
       
        var tl = new TimelineMax();
        tl.set(ad, { force3D: true })

        tl.addLabel('frameOne' , '+=0.58')
        tl.add(logoAnimation, 'frameOne')
        tl.add(imageAnimation, 'frameOne')
        tl.to('#copyOne', 0.2, { y: 0, ease: Power2.easeInOut }, 'frameOne+=0.6');      

        tl.addLabel('frameTwo', '+=2.15')
        tl.to('#copyOne', 0.1, { opacity: 0, ease: Power2.easeInOut }, 'frameTwo');
        tl.to('#copyTwo', 0.2, { y: 0, ease: Power2.easeInOut }), 'frameTwo';
        tl.to('#copyThree', 0.5, { y: 0, ease: Power2.easeOut }, 'frameTwo+=1.3');
        tl.add(loopAnimation,'frameTwo+=3')
    }

    function imageAnimation() {
        tlImage = new TimelineMax();
        tlImage.addLabel("bgAnimation")
        tlImage.to('#bg', 7.5, { scale: 1, x: -14, y: -1, ease: Sine.easeInOut }, 'bgAnimation');
        tlImage.to('#lewis', 7.5, { scale: 1, rotation: 0.01, ease: Sine.easeInOut }, 'bgAnimation');
    }


    function logoAnimation() {
        
        Logo_tl.play();
        Logo_tl.addLabel('logoAnimationOne');
        Logo_tl.to(circle, 1, { morphSVG: oval, ease: Power2.easeInOut },'logoAnimationOne');
        Logo_tl.to(ring, 1, { opacity: 0, ease: Power2.easeInOut }, 'logoAnimationOne');
        Logo_tl.to(YTIcon, 1, { rotation: 360, x: 146, y: 0, scale: 0.65, ease: Power2.easeInOut }, 'logoAnimationOne');
        Logo_tl.to(YTMusicCopy, 0.75, { x: 158, y: 0, ease: Power2.easeInOut }, 'logoAnimationOne+=.1');
        Logo_tl.to(YTMusicCopyContainer, 0.75, { x: 170, ease: Power2.easeInOut }, 'logoAnimationOne+=.1');

        Logo_tl.addLabel('logoAnimationTwo', '+=1.45')
        Logo_tl.to(circle, 0.75, { morphSVG: circle, ease: Power2.easeInOut }, 'logoAnimationTwo');
        Logo_tl.to(ring, 0.75, { opacity: 1, ease: Power2.easeInOut }, 'logoAnimationTwo');
        Logo_tl.to(YTIcon, 0.75, { rotation: 600, x: 322, y: -10, scale: 0.65, ease: Power2.easeInOut }, 'logoAnimationTwo');
        Logo_tl.to(YTMusicCopy, 0.55, { x: 0, y: -10, scale:0.7, ease: Power2.easeInOut },'logoAnimationTwo');
        Logo_tl.to(YTMusicCopyContainer, 0.55, { x: 263, ease: Power2.easeInOut },'logoAnimationTwo');

        Logo_tl.to(playNowCopy, 0.7, { x: 166,rotation:0.01, ease: Power2.easeInOut }, 'logoAnimationTwo');
        Logo_tl.to(playNowContainer, 0.7, { x: 120, y: -10, rotation: 0.01, width:149, ease: Power2.easeInOut }, 'logoAnimationTwo');
       
    }
    function loopAnimation(){
        if(currentLoop < totalLoops ){
            var tll = new TimelineMax();
            tll.to(['#copyTwo','#copyThree','#YTLogo'],0.5,{opacity: 0, ease: Power2.easeInOut})
            tll.set(['#lewis','#bg','#copyOne','#copyTwo','#copyThree','#YTLogo'],{clearProps:'all'})
           
            tll.add(function(){
                Logo_tl.pause(0); 
                tlImage.pause(0);    
            })
            tll.add(play,0.5)
            
            currentLoop++;
        }
    }
   
    function clickHandler(e){
        e.preventDefault();
        window.open(window.clickTag)
    }

}();
