var gulp = require('gulp'),
plumber = require('gulp-plumber'),
useref = require('gulp-useref');


  /*--------------------------------------------------
  MOVE ALL ASSETS INTO THE DEPLOY FOLDER
  --------------------------------------------------*/

module.exports = {

    moveFiles : function (){
        return gulp.src(["src/*", 'src/img/*', 'src/fonts/*', 'src/css/fonts/*', 'src/css/*', '!src/scss/', '!src/index.html'], {
            base: './src',
            nodir: true
          })
          .pipe(plumber())
          .pipe(gulp.dest('deploy'));
    },

    moveHTML : function (){
      return gulp.src('src/index.html')
      .pipe(useref())
      .pipe(gulp.dest('deploy'));
    }
    
}