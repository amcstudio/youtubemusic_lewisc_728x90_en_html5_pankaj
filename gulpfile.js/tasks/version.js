
var gulp = require('gulp'), 
    confirm = require('gulp-confirm'),
    bump = require('gulp-bump'),
    zip = require('gulp-zip'),
    fs = require('fs'),
    path = require('path'),
    thisVersion,
    json,
    filePath = path.basename(path.dirname(path.dirname(__dirname)));


gulp.task(createSourceArchive);
function createSourceArchive() {
    json = JSON.parse(fs.readFileSync('./package.json'))
    return gulp.src('src/**/*')
        .pipe(zip(filePath + "-versionArchive-v" + json.version + '.zip'))
        .pipe(gulp.dest('./_versionArchive/'));
}



function bumpVersion(){
    gulp.src('./package.json')
      .pipe(bump({
        version: thisVersion
      }))
      .pipe(gulp.dest('./'));
   
  }






module.exports = function (){
  
    json = JSON.parse(fs.readFileSync('./package.json'))

    return gulp.src('src/**/*')
      .pipe(confirm({
        question: 'Version Number?, current version is ' + json.version + ' (press enter if you are re-opening the same file)',
        proceed: function (answer) {
          if (answer !== "" && answer !== json.version && json.version !== "0.0.0") {
            createSourceArchive();
          }
          thisVersion = answer || json.version;
          bumpVersion();
          return true;
        }
      }))
  
}

